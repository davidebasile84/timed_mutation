@echo off
REM this script must be located,together with the mutants, in the folder bin-Win32 of Uppaal TIGA or Ecdar
REM TMI, TAD-C, TAD-U, SMI,  CXL-L, CXL-T, CXS-L, CXS-T, CCN
set "mutant="
set "loopcount="
set mutant=TMI
set loopcount=88
@echo  -- %time% > %mutant%_log.txt
@echo TMI
:loop1
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop1
goto loop1
:exitloop1
@echo %time% >> %mutant%_log.txt
set "mutant="
set "loopcount="
set mutant=TAD-C
set loopcount=289
@echo  -- %time% > %mutant%_log.txt
@echo TAD-C
:loop2
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop2
goto loop2
:exitloop2
@echo %time% >> %mutant%_log.txt
set "mutant="
set "loopcount="
set mutant=TAD-U
set loopcount=289
@echo  -- %time% > %mutant%_log.txt
@echo TAD-U
:loop3
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop3
goto loop3
:exitloop3
@echo %time% >> %mutant%_log.txt
set "mutant="
set "loopcount="
set mutant=SMI
set loopcount=17
@echo  -- %time% > %mutant%_log.txt
@echo SMI
:loop5
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop5
goto loop5
:exitloop5
@echo %time% >> %mutant%_log.txt
set "mutant="
set "loopcount="
set mutant=CXL-L
set loopcount=11
@echo  -- %time% > %mutant%_log.txt
@echo CXL-L
:loop7
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop7
goto loop7
:exitloop7
@echo %time% >> %mutant%_log.txt
set "mutant="
set "loopcount="
set mutant=CXL-T
set loopcount=9
@echo  -- %time% > %mutant%_log.txt
@echo CXL-T
:loop8
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop8
goto loop8
:exitloop8
@echo %time% >> %mutant%_log.txt
set "mutant="
set "loopcount="
set mutant=CXS-L
set loopcount=11
@echo  -- %time% > %mutant%_log.txt
@echo CXS-L
:loop9
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop9
goto loop9
:exitloop9
@echo %time% >> %mutant%_log.txt
set "mutant="
set "loopcount="
set mutant=CXS-T
set loopcount=9
@echo  -- %time% > %mutant%_log.txt
@echo CXS-T
:loop10
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop10
goto loop10
:exitloop10
@echo %time% >> %mutant%_log.txt
set "mutant="
set "loopcount="
set mutant=CCN
set loopcount=9
@echo  -- %time% > %mutant%_log.txt
@echo CCN
:loop11
echo  -- mutant_%mutant%_%loopcount% >> %mutant%_log.txt
verifytga.exe mutant_%mutant%_%loopcount%.xml query.q >> %mutant%_log.txt
set /a loopcount=loopcount-1
if %loopcount%%==0 goto exitloop11
goto loop11
:exitloop11
@echo %time% >> %mutant%_log.txt