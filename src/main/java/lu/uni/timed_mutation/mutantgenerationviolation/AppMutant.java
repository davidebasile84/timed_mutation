package lu.uni.timed_mutation.mutantgenerationviolation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;


/*
(1) the SUT shall not be redundant; 

(2) TMI shall not be applied to uncontrollable transitions;

(3) TAD shall not be applied to controllable transitions; 

(4) SMI shall not be applied when all incoming transitions are uncontrollable;

(5) CXL-T shall not be applied to controllable transitions with guards of the form x<=k;
(6) CXL-T shall not be applied to uncontrollable transitions with guards either (i)
x>=k or (ii) x==k and source invariant x<=k; 

(7) CXL-L can be applied to invariants of the form x<=k;
(8) CXL-L shall not be applied to invariants of the form x>=k whenever all incoming transitions are uncontrollable;

(9) CXS-T shall not be applied to controllable transitions with guards of the form x>=k;
(10) CXS-T shall not be applied to uncontrollable transitions with guards of the form x<=k;

(11) CXS-L shall not be applied to invariants of the form x<=k whenever all incoming transitions are uncontrollable;
(12) CXS-L shall not be applied to invariants of the form x>=k whenever all incoming transitions are controllable.
*/

public class AppMutant {
	
	private static File file;
	private static DocumentBuilder docBuilder;
	private static Transformer transformer;
	private static DOMSource source;
	
	private static String fileName = null;
	private static String output = ".";
	private static String violationslog = "Violations.txt";
	private static boolean secondOrder = false;
	private static boolean exhaustive = true;
	
	public static String pathSeparator = System.getProperty("os.name").toLowerCase().contains("win")?"\\":"/";
	private static String mutantpath = ".." + pathSeparator;
	private static String casestudy="COFFEE";

	private static String usage="Usage : java -jar AppMutant.jar [options] -i model.xml"+System.lineSeparator()+""
			+ "where options are :"+System.lineSeparator()+""
			+ "\t-mutantpath [path] to indicate the path where the directories of the mutants will be generated, default is "+mutantpath+System.lineSeparator()
			+ "\t-casestudy [COFFEE|CAS|WFAS|ACCEL|MUTEX|HOTEL] to select one of the available case studies, default is: "+casestudy+System.lineSeparator()
			//	+ "\t-o folder to indicate the folder where mutants will be generated"+System.lineSeparator()+""
			+ "\t-secondorder to generate second order mutants"+System.lineSeparator()+""
			+ "\t-sampling to generate samples (10%) of second order mutants"+System.lineSeparator()+"";
//			+ "\t-log to indicate the filename of the guidelines violations log.";
	

	public static void main(String argv[]) {

		for(int i = 0; i < argv.length; i++) {
			if(argv[i].equals("-i")) {
				fileName = argv[++i];
			}
			else if (argv[i].equals("-mutantpath")) {
				mutantpath= argv[++i];
			}

			else if (argv[i].equals("-casestudy")) {
				if (!(casestudy.equals("COFFEE")||casestudy.equals("CAS")||casestudy.equals("WFAS")
						||casestudy.equals("ACCEL")||casestudy.equals("MUTEX")||casestudy.equals("HOTEL")))
				{
					System.out.println("Case study not available");
					return;
				}
				casestudy = argv[++i];			
			}

//			else if(argv[i].equals("-o")) {
//				output = argv[++i];
//			}
			else if(argv[i].equals("-secondorder")) {
				secondOrder = true;
			}
			else if(argv[i].equals("-sampling")) {
				exhaustive = false; 
			}
//			else if (argv[i].equals("-log")) {
//				violationslog = argv[++i];
//			}
			else {
				System.out.println("Invalid option");
				return;
			}
		}
		
		String order = (secondOrder)?"2":"1";
		
		output=mutantpath + pathSeparator + casestudy + order + pathSeparator;
		violationslog = casestudy+order+"_Violations.txt";
		
	//	if(!output.equals("."))
			try {
				Files.createDirectories(Paths.get(output));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		try {

			AppMutant app = new AppMutant();
			file = app.loadFileFromResources(fileName);
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docFactory.newDocumentBuilder();
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			

			//long start = System.currentTimeMillis();
			if(!secondOrder)
				runFirstOrder(app);
			else
				runSecondOrder(app, exhaustive);
			//long end = System.currentTimeMillis();
			//System.out.println(end - start);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(usage);
			return;
		}
	}
	
	private Model original() throws SAXException, IOException {
		
		Document doc = docBuilder.parse(file);
		source = new DOMSource(doc);
		Node root = doc.getFirstChild();
		return new Model(root);
	}
	
	private static void runFirstOrder(AppMutant app) throws TransformerException, SAXException, IOException {
		
		Mutation tmi = new TransitionMissing(); // 88
		Mutation tadc = new TransitionAddedControllable(); // 289
		Mutation tadu = new TransitionAddedUncontrollable(); // 289
		Mutation smi = new StateMissing(); // 17
		Mutation cxlt = new ConstraintLargerTransition(); // 9
		Mutation cxst = new ConstraintSmallerTransition(); // 9
		Mutation cxll = new ConstraintLargerLocation(); // 11
		Mutation cxsl = new ConstraintSmallerLocation(); // 11
		Mutation ccn = new ConstraintNegationTransition(); // 9
		
		Mutation[] mutations = { tmi, tadc, tadu, smi, cxlt, cxst, cxll, cxsl, ccn };
		
		//Model m =
				app.original();
				
		StringBuilder sb_v = new StringBuilder();
				
		for(int i = 0; i < mutations.length; i++) { 
			Map.Entry<Integer, String> e = app.runOneMutation(mutations[i]);
			sb_v.append(mutations[i].name()+","+e.getKey()+System.lineSeparator());
			app.printToFile(mutations[i].name()+"_violations.txt", e.getValue());
		}

		app.printToFile(violationslog, sb_v.toString());
		app.printToFile("query1.q", "refinement : Spec_mutant <= Spec");
	}

	private static void runSecondOrder(AppMutant app, boolean exhaustive) throws TransformerException, SAXException, IOException {
		
		if (exhaustive) {
			System.out.println("Second order mutants exhaustive generation and violations checking. "+System.lineSeparator()+" "
					+ "Warning! This operation may take a while. "+System.lineSeparator()+" "
					+ "Press any key to continue");
			Scanner scan = new Scanner(System.in); 
			scan.nextLine();
			scan.close();
		
		}
		Mutation tmi = new TransitionMissing(); // 88
		Mutation tadc = new TransitionAddedControllable(); // 289
		Mutation tadu = new TransitionAddedUncontrollable(); // 289
		Mutation smi = new StateMissing(); // 17
		Mutation cxlt = new ConstraintLargerTransition(); // 9
		Mutation cxst = new ConstraintSmallerTransition(); // 9
		Mutation cxll = new ConstraintLargerLocation(); // 11
		Mutation cxsl = new ConstraintSmallerLocation(); // 11
		Mutation ccn = new ConstraintNegationTransition(); // 9
		
		Mutation[] mutations = { tmi, tadc, tadu, smi, cxlt, cxll, cxst, cxsl, ccn };
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < mutations.length; i++) 
			for(int j = 0; j < mutations.length; j++) {
				    Map.Entry<Integer, Integer> v = exhaustive? app.runtwoMutations(mutations[i], mutations[j]): app.runtwoMutationsSampling(mutations[i], mutations[j]);
				    sb.append(mutations[i].name()+"_"+mutations[j].name()+","+v.getKey()+","+v.getValue()+System.lineSeparator());
			}
		app.printToFile(violationslog, sb.toString());

		app.printToFile("query3.q", "refinement : Spec_mutant <= Spec");
		app.printToFile("query2.q", "refinement : Spec_mutant_mutant <= Spec_mutant");
		app.printToFile("query1.q", "refinement : Spec_mutant_mutant <= Spec");
	}

	
	private AbstractMap.Entry<Integer,Integer> runtwoMutationsSampling(Mutation m1, Mutation m2) throws TransformerException, SAXException, IOException {
		
		int v = 0;
		boolean v2 = false;
		int tot=0;
		
		Random rand = new Random(12345);
		
		Model ori = original();
		int n1 = getPossibleMutations(m1,ori); 
		int n2 = getPossibleMutations(m2,ori);
				
		int samples_emp = n1 * n2 * 10/100; 
		samples_emp = samples_emp > 1000? (int) ((n1 * n2) * 1/100) : samples_emp;
		
		//System.out.println(samples_emp);
		
		for(int k = 0; k < samples_emp; k++) {
		
			ori = original();
			Mutator mutator_1 = new Mutator(ori);
			int i = rand.nextInt(n1);
			
			boolean v1 = mutator_1.mutate(m1, i, false);
			if (v1) 
				tot++;
			
			// Applying the actual two mutations
			//System.out.println("----- second order mutation "+i+"/"+j+" -----");
			
			Model mutant = mutator_1.getMutant();
			int j = rand.nextInt(getPossibleMutations(m2,mutant));
			Mutator mutator_2 = new Mutator(mutant);
			v2 = mutator_2.mutate(m2, j, false);
			
			//if the second mutation violates their guidelines whilst the first not the sec. ord. mutant is violating the guideline
			if(v1 && v2)
				v++;
			
			ori.addAutomata(mutant);
			ori.addAutomata(mutator_2.getMutant());
			StreamResult result = new StreamResult(storeFileFromResources("mutant_"+m1.name()+"_"+(i+1)+"_"+m2.name()+"_"+(j+1)+".xml"));
			transformer.transform(source, result);
		
		    //System.out.println("mutant_"+m1.name()+"_"+(i+1)+"_"+m2.name()+"_"+(j+1)+" violations = "+v);
		}

		return new SimpleEntry<Integer,Integer>(v,tot);
	}
	

    private Map.Entry<Integer, Integer> runtwoMutations(Mutation m1, Mutation m2) throws TransformerException, SAXException, IOException {
		
		int v = 0;
		int tot=0;
		
		boolean v1 = false;
		boolean v2 = false;
		
		Model ori = original();
		int n1 = getPossibleMutations(m1,ori); 
		for(int i = 0; i < n1; i++) {
			
			ori = original();
			Mutator mutator_1 = new Mutator(ori);
			v1 = mutator_1.mutate(m1, i, false);
			
			int n2 = getPossibleMutations(m2,mutator_1.getMutant());
			
			for(int j = 0; j < n2; j++) {
				//j!=i 
						
				ori = original();
				mutator_1 = new Mutator(ori);
				v1 = mutator_1.mutate(m1, i, false);					
				
				Model mutant = mutator_1.getMutant();				
				
				Mutator mutator_2 = new Mutator(mutant);
				
				v2 = mutator_2.mutate(m2, j, false);
				
				if(v1 && v2)
					v++;
				
				ori.addAutomata(mutant);
				ori.addAutomata(mutator_2.getMutant());
				StreamResult result = new StreamResult(storeFileFromResources("mutant_"+m1.name()+"_"+(i+1)+"_"+m2.name()+"_"+(j+1)+".xml"));
				transformer.transform(source, result);
			}
		}
		
		return new SimpleEntry<Integer,Integer>(v,tot);
	}
    
//    private AbstractMap.Entry<Integer,Integer> runtwoMutations(Mutation m1, Mutation m2) throws TransformerException, SAXException, IOException {
//		
//		int v = 0;
//		int tot=0;
//		
//		boolean v2 = false;
//		
//		Model ori = original();
//		int n1 = ori.getPossibleMutations(m1);
//		for(int i = 0; i < n1; i++) {
//			
//			ori = original();
//			Mutator mutator_1 = new Mutator(ori);
//			
//			int n2 = mutator_1.getMutant().getPossibleMutations(m2);
//
//			boolean violation_first_order = mutator_1.mutate(m1, i, false);
//			
//			if (!violation_first_order)
//				tot = tot + n2; //number of second order mutants whose first order mutant is not violating guideline
//			
//			for(int j = 0; j < n2; j++) {
//				
//				ori = original();
//				mutator_1 = new Mutator(ori);
//				mutator_1.mutate(m1, i, false);					
//				
//				Model mutant = mutator_1.getMutant();
//				Mutator mutator_2 = new Mutator(mutant);
//				v2 = mutator_2.mutate(m2, j, false);
//				
//				if(!violation_first_order && v2)
//					v++;
//				
//				ori.addAutomata(mutant);
//				ori.addAutomata(mutator_2.getMutant());
//				StreamResult result = new StreamResult(storeFileFromResources("mutant_"+m1.name()+"_"+(i+1)+"_"+m2.name()+"_"+(j+1)+".xml"));
//				transformer.transform(source, result);
//				
//				
//			}
//		}
//		
//		return new SimpleEntry<Integer,Integer>(v,tot);
//	}
	
	private Map.Entry<Integer, String> runOneMutation(Mutation mutation) throws TransformerException, SAXException, IOException {
		
		int number = getPossibleMutations(mutation,original());
		int v = 0;
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < number; i++) {//original.transitions().size(); i++) {
			// Copying the whole XML tree

		    Model ori = original();
			Mutator mutator = new Mutator(ori);
			if(mutator.mutate(mutation, i, false))
			{
				sb.append(mutation.name()+"_"+(i+1) + System.lineSeparator());
				v++;
			}
				
			ori.addAutomata(mutator.getMutant());
			StreamResult result = new StreamResult(storeFileFromResources( "mutant_"+mutation.name()+"_"+(i+1)+".xml"));
			transformer.transform(source, result);
		}
		
		return new AbstractMap.SimpleEntry<Integer,String>(v,sb.toString());
	}
	
	private File loadFileFromResources(String fileName) throws IOException {
		return new File(fileName);
	}
	
	private File storeFileFromResources(String fileName) {
		try {
			Files.createFile(Paths.get(output+"/"+fileName));
		} catch (IOException e) {
		}
		return new File(output+"/"+fileName);

	}

	private void printToFile(String fileName, String content) throws IOException {
		try (FileWriter logWriter = new FileWriter(Paths.get(output+"/"+fileName).toString())){
			logWriter.append(content);
		}
	}
	

	private static int getPossibleMutations(Mutation mutation, Model m) {
		return mutation.getNumberOfMutations(m);
	}
}