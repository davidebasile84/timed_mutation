package lu.uni.timed_mutation.mutantgenerationviolation;

public interface Mutation {
	
	public boolean apply(Model m, int index);
	
	public String name();
	
	public int getNumberOfMutations(Model m);

}
