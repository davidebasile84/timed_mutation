package lu.uni.timed_mutation.mutantgenerationviolation;

import java.util.List;

import org.w3c.dom.Node;

import lu.uni.timed_mutation.mutantgenerationviolation.Model.GuardKind;

public class ConstraintLargerLocation extends ConstraintLargerMutation {
	
	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);

		List<Node> locations = m.locationsWithInvariant();
		Node loc = locations.get(index);
		Node invariant = m.invariant(loc);
		
		//String locId = loc.getAttributes().getNamedItem("id").getTextContent();
		//System.out.println(name() +"#" + count + " Increased invariant location " + locId);
		
		boolean violated = false;
		if((Model.guardkind(invariant) == GuardKind.GEQ || Model.guardkind(invariant) == GuardKind.GT) && m.incomingTransitionsUncontrollable(loc)) {
			violated = true;
		}
		
		this.raiseBound(invariant);
		
		return violated;
	}

	@Override
	public String name() {
		return "CXL-L";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.locationsWithInvariant().size();
	}

}
