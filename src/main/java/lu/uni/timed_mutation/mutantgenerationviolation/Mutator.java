package lu.uni.timed_mutation.mutantgenerationviolation;

public class Mutator {
	
	private Model model = null;
	
	public Mutator(Model model) {
		this.model = model;		
	}
	
	public boolean mutate(Mutation m, int i, boolean erase) {
		if(!erase)
		{
			model.root().cloneNode(true);
			model = new Model(model.root().cloneNode(true));
		}
		else
			model = new Model(model.root());
		
		return m.apply(model, i);
	}
	
	public Model getMutant() {
		return model;
	}
	
}
