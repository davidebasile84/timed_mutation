package lu.uni.timed_mutation.mutantgenerationviolation;

import java.util.List;

import org.w3c.dom.Node;

public class StateMissing implements Mutation {
	
	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		Node automaton = m.automata().get(0);
		List<Node> locations = m.locationsWithoutInitial(automaton);
		Node location = locations.get(index);
		
		boolean violated = false;
		if(m.incomingTransitionsUncontrollable(location)) {
			violated = true;
		}
		
		automaton.removeChild(location);
		removeTransitions(m, location);
		
		//System.out.println(name() +"#" + index + " Removed location " + locId + (violated? " violates the guidelines": " does not violate the guidelines"));
		
		return violated;
	}

	@Override
	public String name() {
		return "SMI";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.locationsWithoutInitial(m.automata().get(0)).size();
	}
	
	private void removeTransitions(Model m, Node location) {
		Node automaton = m.automata().get(0);
		List<Node> transitions = m.transitions(automaton);
		String locId = location.getAttributes().getNamedItem("id").getTextContent();
		for (int j = 0; j < transitions.size(); j++) {
			Node transition = transitions.get(j);
			if (locId.contentEquals(Model.children(transition, "source").get(0).getAttributes().getNamedItem("ref").getTextContent()) 
					|| locId.contentEquals(Model.children(transition, "target").get(0).getAttributes().getNamedItem("ref").getTextContent())) {
				automaton.removeChild(transition);
			}
		}
		
	}

}
