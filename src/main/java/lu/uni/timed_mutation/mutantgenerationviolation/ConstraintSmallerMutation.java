package lu.uni.timed_mutation.mutantgenerationviolation;

import org.w3c.dom.Node;

public abstract class ConstraintSmallerMutation implements Mutation{
	public void reduceBound(Node constraint) {
		String asText = constraint.getTextContent();

		int index;
		if (asText.contains("==")) {
			index = asText.indexOf("==") + 2;
		} else if (asText.contains("=")) {
			index = asText.indexOf("=") + 1;
		} else if (asText.contains("<")) {
			index = asText.indexOf("<") + 1;
		} else if (asText.contains(">")) {
			index = asText.indexOf(">") + 1;
		} else {
			index = asText.indexOf(";") + 1;
		}
		String boundAsText = asText.substring(index);
		int bound = Integer.parseInt(boundAsText.replaceAll("\\s+",""));

		int newBound = bound - 1;
		constraint.setTextContent(asText.substring(0, index) + newBound);
	}
}
