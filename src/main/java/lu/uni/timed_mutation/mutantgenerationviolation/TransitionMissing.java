package lu.uni.timed_mutation.mutantgenerationviolation;

import java.util.List;

import org.w3c.dom.Node;

public class TransitionMissing implements Mutation {
	
	@Override
	public boolean apply(Model m, int index) {
		assert(index < getNumberOfMutations(m) && getNumberOfMutations(m) > 0);
		
		List<Node> transitions = m.transitions();
		Node transition = transitions.get(index);
	    m.automata().get(0).removeChild(transition);
				
		//String source = Model.children(transition, "source").get(0).getAttributes().getNamedItem("ref").getTextContent();
		//String target = Model.children(transition, "target").get(0).getAttributes().getNamedItem("ref").getTextContent();
		//System.out.println(name() + "#" + (index+1) + " Removed transition: " + source + " -> " + target + " / " +transition.getTextContent());
		
	    boolean violated = false;
	    
	    //Uppaal does not specify the attribute controllable when it is true, only when it is false
	    Node test = transition.getAttributes().getNamedItem("controllable");
	    if(test!=null && test.getNodeValue().equals("false")) {
			violated = true;
		}
		
		
		return violated;
	}

	@Override
	public String name() {
		return "TMI";
	}

	@Override
	public int getNumberOfMutations(Model m) {
		return m.transitions().size();
	}

}
