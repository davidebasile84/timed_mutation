package lu.uni.timed_mutation.refinementparsing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import lu.uni.timed_mutation.refinementparsing.AppEcdar.MutantValue;

public class LogParser implements Consumer<Integer>{
	private final String mutantpath;
	private final String casestudy;

	public LogParser(String mutantpath,String casestudy) {
		this.mutantpath=mutantpath;
		this.casestudy=casestudy;
	}

	@Override
	public void accept(Integer orderint) throws RuntimeException{
		String order=(orderint==2)?"second":"first";
		String preamble = "\\begin{table}[tb]"+System.lineSeparator()+"" + 
				"\\centering "+System.lineSeparator()+"" + 
				"\\caption{\\label{tab:"+casestudy+orderint+"}"+casestudy+" "+ order +"-order mutants results.}"+System.lineSeparator()+"" + 
				"\\setlength{\\tabcolsep}{3pt}"+System.lineSeparator()+"" + 
				"\\def\\arraystretch{1.2}"+System.lineSeparator()+"" + 
				"\\scalebox{1}{\\begin{tabular}{|@{\\hskip1pt}l@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|}"+System.lineSeparator()+"" + 
				"\\hline"+System.lineSeparator()+"" + 
				"\\multirow{2}{*}{\\diagbox[trim=lr]"+System.lineSeparator()+"" + 
				"{\\small operator~}{\\small measure}}"+System.lineSeparator()+"" + 
				"& \\multirow{2}{*}{\\small mutants} & \\multirow{2}{*}{$\\stackrel{\\text{\\small non-}}{\\text{\\small subsumed}}$} "+System.lineSeparator()+"" + 
				"& \\multirow{2}{*}{\\small{subsumed}} "+System.lineSeparator()+"" + 
				"& \\multirow{2}{*}{{\\small inconsistent}} "+System.lineSeparator()+"" + 
				"& \\multirow{2}{*}{$\\stackrel{\\text{\\small violating}}{\\text{\\small guidelines}}$} &"+System.lineSeparator()+"" + 
				"\\multirow{2}{*}{$\\frac{\\text{\\small violating}}{\\stackrel{\\text{\\small subsumed +}}{\\text{\\small inconsistent}}}$} \\\\ "+System.lineSeparator()+"" + 
				"& & & & & & \\\\ \\hline ";

		
		Map<String, List<Integer>> mapViolations = parseViolationsLog(orderint);

		Map<String, List<File>> mapfile=null;
		try {
			mapfile = readFilesIntoMap(Paths.get(mutantpath),"_");
		} catch (IOException e1) {
			RuntimeException re = new RuntimeException();
			re.addSuppressed(e1);
			throw re;
		}
		
		//computing for each log the number of subsumed, nonsubsumed and inconsistent mutants
		Map<String, Map<AppEcdar.MutantValue, Long>> parseMap = mapfile.entrySet().stream()
				.filter(x->x.getKey()!="")
				.collect(Collectors.toMap(Entry::getKey, e-> parseMutantLogToMap(e.getKey(),orderint))); 
		
//		parseMap.entrySet().stream()
//		.filter(e->e.getKey().substring(0, 3).contains("TAD") 
//				&&e.getKey().substring(3).contains("TAD") )
//		.forEach(e->System.out.println(e.getKey()+" "+e.getValue()));


		List<String> out = new ArrayList<>();

		Map<String, List<Map<AppEcdar.MutantValue, Long>>>  groupedParseMap = parseMap.entrySet().stream()
				.collect(Collectors.groupingBy(e->
				{String[] s = e.getKey().split("_");
				return (s.length==1)?s[0].substring(0,3): //first order
					(s.length==2)?s[0].substring(0,3)+"-"+s[1].substring(0,3):"";}, //second order 
				Collectors.mapping(Entry::getValue,Collectors.toList())));  


		DecimalFormat df = new DecimalFormat("#");
		Long tot_nonsubsumed=(long)0;
		Long tot_subsumed=(long)0;
		Long tot_inconsistent=(long)0;
//		Integer tot_nonviolations=0;
		Long tot_violations=(long)0;

		for (Entry<String, List<Map<MutantValue, Long>>> e : groupedParseMap.entrySet())
		{
			Long nonsubsumed=e.getValue().stream()
					.map(m -> m.entrySet().stream()
							.filter(ee->ee.getKey().equals(MutantValue.NONSUBSUMED))
							.map(Entry::getValue)
							.collect(Collectors.summingLong(Long::longValue)))
					.collect(Collectors.summingLong(Long::longValue));

			Long subsumed=e.getValue().stream()
					.map(m -> m.entrySet().stream()
							.filter(ee->ee.getKey().equals(MutantValue.SUBSUMED))
							.map(Entry::getValue)
							.collect(Collectors.summingLong(Long::longValue)))
					.collect(Collectors.summingLong(Long::longValue));

			Long inconsistent=e.getValue().stream()
					.map(m -> m.entrySet().stream()
							.filter(ee->ee.getKey().equals(MutantValue.INCONSISTENT))
							.map(Entry::getValue)
							.collect(Collectors.summingLong(Long::longValue)))
					.collect(Collectors.summingLong(Long::longValue));


			Integer violations = mapViolations.get(e.getKey()).get(0);

			tot_nonsubsumed+=nonsubsumed;
			tot_subsumed+=subsumed;
			tot_inconsistent+=inconsistent;
			tot_violations+=violations;

			StringBuilder sbtex = new StringBuilder();

			sbtex.append("\\texttt{"+e.getKey()+"} & ");
			sbtex.append((nonsubsumed+subsumed+inconsistent)  + " & ");
			sbtex.append(nonsubsumed+"   & ");
			sbtex.append((subsumed)+"  &");
			sbtex.append((inconsistent)+"  & ");
			sbtex.append(violations+"  & ");

			if (subsumed+inconsistent!=0)
				sbtex.append(df.format(((double)violations/(subsumed+inconsistent))*100) +"\\%  ");
			else
				sbtex.append(" - ");


			sbtex.append("  \\\\ \\hline"+System.lineSeparator());

			out.add(sbtex.toString());

		};

		Comparator<String> comp = (orderint==1)?(a,b)->a.substring(8, 11).compareTo(b.substring(8,11)):(a,b)->a.substring(8, 15).compareTo(b.substring(8,15));

		out.sort(comp.reversed());

		StringBuilder sbtex = new StringBuilder();


		sbtex.append("totals & "); 
//		if (orderint==1)
			sbtex.append((tot_nonsubsumed+tot_subsumed+tot_inconsistent)  + " & ");
		sbtex.append(tot_nonsubsumed+ " & ");
		sbtex.append((tot_subsumed)+"   & ");
		sbtex.append((tot_inconsistent)+"  & ");
		sbtex.append(tot_violations+" & ");

		if (tot_subsumed+tot_inconsistent!=0)
			sbtex.append(df.format(((double)tot_violations/(tot_subsumed+tot_inconsistent))*100) +"\\%  ");
		else
			sbtex.append(" - ");
		sbtex.append("  \\\\ \\hline"+System.lineSeparator());
		sbtex.append("\\end{tabular}}"
				+ "\\end{table}");

		order=(order=="second")?"Second":"First";

		try (FileWriter logWritter = new FileWriter(casestudy+"_"+order+"Order_CoarseResults.tex",false)){
			logWritter.write(preamble);
			logWritter.write(out.stream()
					.collect(Collectors.joining()));
			logWritter.write(sbtex.toString());
			logWritter.close();
		}	catch (IOException e1) {
			RuntimeException re = new RuntimeException();
			re.addSuppressed(e1);
			throw re;
		}
	}

	private Map<AppEcdar.MutantValue, Long> parseMutantLogToMap(String mutant,int order)  {
		String file;
		String violating="";
		try {
			file = new String (Files.readAllBytes( Paths.get(mutantpath+mutant+"_log.txt")));
			if (order==1)
				violating = new String (Files.readAllBytes( Paths.get(mutantpath+mutant+"_violations.txt")));
		} catch (IOException e1) {
			System.out.println(e1.toString());
			e1.printStackTrace();
			return null;
		}

		
		//inconsistent comes first, otherwise it could be counted as subsumed
		String[] mutants = file.split("mutant_");
		Map<String,List<MutantValue>> map = 
				IntStream.range(1,mutants.length) //discard first element
				.mapToObj(i->mutants[i])
				.collect(Collectors.groupingBy(s-> s.split(System.lineSeparator())[0], 
						Collectors.mapping(s->{return (s.contains("inconsistent")||s.contains("Inconsistent"))?MutantValue.INCONSISTENT
								:s.contains("NOT satisfied")?MutantValue.NONSUBSUMED
										:s.contains("satisfied")?MutantValue.SUBSUMED
												:MutantValue.NIL;},Collectors.toList())));

		if (order==1) {
			checkViolating(mutant, mutants, violating.split(System.lineSeparator()));
		}
		//System.out.println(map.toString());

		Map<AppEcdar.MutantValue, Long> res = new HashMap<>();
		//		if (order==1)
		//{
			res.put(MutantValue.NONSUBSUMED, map.entrySet().stream()
					.filter(e->e.getValue().get(0).equals(MutantValue.NONSUBSUMED))
					.count());
			res.put(MutantValue.SUBSUMED, map.entrySet().stream()
					.filter(e->e.getValue().get(0).equals(MutantValue.SUBSUMED))
					.count());
			res.put(MutantValue.INCONSISTENT, map.entrySet().stream()
					.filter(e->e.getValue().get(0).equals(MutantValue.INCONSISTENT))
					.count());

		return res;
	}

	private void checkViolating(String mutant,String[] mutants, String[] violating)  {
		String falsenegatives = Arrays.stream(violating)
		.filter(v->Arrays.stream(mutants)
				.anyMatch(l->l.contains(v+".xml") && l.contains("NOT satisfied")))
		.collect(Collectors.joining(System.lineSeparator()));
		
		try (FileWriter logWriter = new FileWriter(Paths.get(mutantpath+AppEcdar.pathSeparator+mutant+"_false_negatives.txt").toString())){
			logWriter.append(falsenegatives);
		} catch (IOException e) {
			RuntimeException re = new RuntimeException(e.getMessage());
			re.addSuppressed(e);
			throw re;
		}
	}
	
	private Map<String,List<Integer>> parseViolationsLog(int order) {
		if (order!=1 && order!=2)
			throw new IllegalArgumentException();
		String file;
		try {
			file = new String (Files.readAllBytes( Paths.get(mutantpath+casestudy+order+"_Violations.txt")));
		} catch (IOException e1) {
			System.out.println(e1.toString());
			e1.printStackTrace();
			return null;
		}

		String lines[]=file.split(System.lineSeparator());

		//System.out.println(Arrays.toString(lines));

		Map<String,List<Integer>> parsedMap=		
				Arrays.stream(lines)
				.map(x->x.split(","))
				.filter(x->x.length>1)//possible noise in data
				.collect(Collectors.toMap(x->x[0], x->
				(order==2)?Arrays.asList(Integer.parseInt(x[1]), Integer.parseInt(x[2])) //
						:Arrays.asList(Integer.parseInt(x[1]))));	

		if (parsedMap.size()==0)
			throw new IllegalArgumentException();

		Map<String,List<List<Integer>>> groupedParsedMap=
				parsedMap.entrySet().stream()
				.collect(Collectors.groupingBy(e->
				{String[] s = e.getKey().split("_");
				return (s.length==1)?s[0].substring(0,3): //first order
					(s.length==2)?s[0].substring(0,3)+"-"+s[1].substring(0,3):"";}, //second order 
				Collectors.mapping(Entry::getValue,Collectors.toList()))); 
		//forced by the downstream collector to encapsulate into another list

		Map<String,List<Integer>> result = new HashMap<>();
		for (Entry<String,List<List<Integer>>> e : groupedParsedMap.entrySet()) {
			List<Integer> temp = new ArrayList<>(Arrays.asList(0,0));
			for (List<Integer> li : e.getValue()) {
				temp.set(0, temp.get(0)+li.get(0));
				//				if (order==2) {
				//					temp.set(1, temp.get(1)+li.get(1));
				//				}
			}
			result.put(e.getKey(), temp);
		}

		return result;
	}

	public static Map<String, List<File>> readFilesIntoMap(Path mutantpath,String separator) throws IOException{
		//each mutation or couple of mutations (e.g. TADTMI) is an entry in the map
		return Files.list(mutantpath)
				.map(Path::toFile)
				.filter(f->f.getName().endsWith("xml"))
				.collect(Collectors.groupingBy(x->{
					String[] s =  x.getName().split(Pattern.quote(System.getProperty("file.separator")))[0]//"\\.")[0]
							.split("_");
					return (s.length==3)?s[1]: //first order, e.g. mutant_TMI_84.txt is a filename, it returns TMI
						(s.length==5)?s[1]+separator+s[3]:""; //second order 
						//(s.length>1)?s[1]:"";
				} ));
	}
}








//		String order = (parseMap.entrySet().stream()
//				.map(Entry::getKey)
//				.findFirst()
//				.orElse("")
//				.contains("_"))?"second":"first";





//:
//"\\begin{table}[tb]"+System.lineSeparator()+"" + 
//"\\centering "+System.lineSeparator()+"" + 
//"\\caption{\\label{tab:"+casestudy+orderint+"}"+casestudy+" "+ order +" order mutants results.}"+System.lineSeparator()+"" + 
//"\\setlength{\\tabcolsep}{3pt}\r\n" + 
//"\\def\\arraystretch{1.35}\r\n" + 
//"\\scalebox{1}{\\begin{tabular}{|@{\\hskip1pt}l@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|@{\\hskip1pt}r@{\\hskip1pt}|}\r\n" + 
//"\\hline\r\n" + 
//"%\\multirow{3}{*}{\\diagbox[trim=lr]%\r\n" + 
//"%\\diagbox[innerwidth=1cm,height=1.5cm]\r\n" + 
//"%{\\small operator~}{\\small measure}}\r\n" + 
//"\\multirow{3}{*}{\\small{operator}}\r\n" + 
//"& \\multirow{3}{*}{$\\substack{\\text{\\small \\phantom{l}non-\\phantom{j}}\\\\ \\text{\\small subsumed}}$} \r\n" + 
//"& \\multirow{3}{*}{$\\substack{\\text{\\small \\phantom{l}sub-\\phantom{j}}\\\\ \\text{\\small sumed}}$}\r\n" + 
//"& \\multirow{3}{*}{$\\substack{\\text{\\small \\phantom{l}in-\\phantom{j}}\\\\ \\text{\\small consistent}}$} \r\n" + 
//"& \\multirow{3}{*}{$\\stackrel{\\text{\\small violating}}{\\text{\\small guidelines}}$} \r\n" + 
//"& \\multirow{3}{*}{$\\stackrel{\\text{\\small nonviolating}}{\\text{\\small guidelines}}$} \r\n" + 
//"& \\multirow{3}{*}{$\\substack{\\wideunderline[5.25em]{\\text{\\small violating}}\\\\ \\text{\\small violating\\,+}\\\\ \\text{\\small nonviolating}}\\times\\substack{\\text{\\small inconsistent\\,+}\\\\ \\text{\\small subsumed\\,+}\\\\ \\wideunderline[5em]{\\text{\\small \\phantom{j}nonsubsumed\\phantom{j}}}\\\\ \\text{\\small subsumed\\,+}\\\\ \\text{\\small inconsistent}}$} \\\\ \r\n" + 
//"& & & & & & \\\\ & & & & & &  \\\\ \\hline\r\n" + 
//"";



//			if (orderint==2)
//			{
//				long populationEcdar = nonsubsumed + subsumed + inconsistent;
//
//				Integer populationTool=mapViolations.get(e.getKey()).get(1); //TODO not clear this one, before it was get(1)
//				Integer non_violations = populationTool - violations;
//				tot_nonviolations+=non_violations;
//				sbtex.append(non_violations+" & ");
//
//				if ((subsumed+inconsistent!=0)&&(populationTool!=0))
//				{
//					sbtex.append(
//							df.format(
//									((double)violations/populationTool)*
//									((double)populationEcdar/(subsumed+inconsistent))
//									*100)+"\\%  ");
//				}
//				else
//					sbtex.append(" - ");
//
//			}		
//			else {



//}
//		else if (order==2)
//			//in case of second order, first query (get(0)) must be nonsubsumed, 
//			//note that map is created sequentially, and in refinementChecking the query2 is logged after query1, thus get(0) is the first query
//		{
//			res.put(MutantValue.NONSUBSUMED, map.entrySet().stream()
//					.map(Entry::getValue)
//					.filter(e->e.get(0).equals(MutantValue.NONSUBSUMED))   
//					.filter(e->e.get(1).equals(MutantValue.NONSUBSUMED))
//					.count());
//			res.put(MutantValue.SUBSUMED, map.entrySet().stream()
//					.map(Entry::getValue)
//					.filter(e->e.get(0).equals(MutantValue.NONSUBSUMED))
//					.filter(e->e.get(1).equals(MutantValue.SUBSUMED))
//					.count());
//			res.put(MutantValue.INCONSISTENT, map.entrySet().stream()
//					.map(Entry::getValue)
//					.filter(e->e.get(0).equals(MutantValue.NONSUBSUMED))
//					.filter(e->e.get(1).equals(MutantValue.INCONSISTENT))
//					.count());
//		}



//if (orderint==2)
//{
//	sbtex.append(tot_nonviolations+" & ");
//	long tot_populationTool = tot_violations + tot_nonviolations;
//	long tot_populationEcdar = tot_nonsubsumed+tot_subsumed+tot_inconsistent;
//	if ((tot_subsumed+tot_inconsistent!=0)&&(tot_populationTool!=0))
//	{
//		sbtex.append(
//				df.format(
//						((double)tot_violations/tot_populationTool)*
//						((double)tot_populationEcdar/(tot_subsumed+tot_inconsistent))
//						*100)+"\\%  ");
//	}
//	else
//		sbtex.append(" - ");
//
//}	
//else 
