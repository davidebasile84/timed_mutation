package lu.uni.timed_mutation.refinementparsing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class RefinementChecker implements Consumer<Integer> { 
	private final String mutantpath;
	private final String ecdarpath;

	public RefinementChecker(String mutantpath,String ecdarpath) {
		this.mutantpath=mutantpath;
		this.ecdarpath=ecdarpath;
	}

	@Override
	public void accept(Integer order) throws RuntimeException {
		String OS = System.getProperty("os.name").toLowerCase();
		
		Map<String, List<File>> mapfile=null;
		try {
			mapfile = LogParser.readFilesIntoMap(Paths.get(mutantpath),"");
		} catch (IOException e1) {
			RuntimeException re = new RuntimeException();
			re.addSuppressed(e1);
			throw re;
		}

		//all mutations are processed in parallel
		mapfile.entrySet().parallelStream()
		.filter(x->x.getKey()!="")
		.map(Entry::getValue)
		//.limit(2)  //just for debugging
		.forEach(list->
		{
			String[] temp = list.get(0).getName().split(Pattern.quote(System.getProperty("file.separator")))[0]
					.split("_");
			String logfilename = (temp.length==3)?temp[1]: //firstorder
				(temp.length==5)?temp[1]+"_"+temp[3]:"error"; //secondorder

				logfilename+="_log.txt";

				System.out.println("Start logging "+logfilename+" at "+LocalTime.now());

				StringBuilder sb = new StringBuilder();
				sb.append("-- "+LocalTime.now()+System.lineSeparator());
				for (File f : list) //all mutants belonging to that mutation 
				{
					String filename = f.getName().split(Pattern.quote(System.getProperty("file.separator")))[0];
//					if (order==1)
//					{
						sb.append(filename+System.lineSeparator());
						String cmd= (OS.contains("win"))?
								String.format("cmd /c "+ecdarpath+
										"verifytga.exe -s %s "+mutantpath+"query1.q",f.toString()):
											String.format(ecdarpath+"verifytga -s %s "+mutantpath+"query1.q",f.toString());

								try {
									Process process = Runtime.getRuntime().exec(cmd);
									BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
									String s;
									while ((s = stdInput.readLine()) != null) {
										sb.append(s);
									}
									sb.append(""+System.lineSeparator()+"");
								} catch (IOException e) {
									//  Auto-generated catch block
									e.printStackTrace();
								}
				} 
				try (FileWriter logWriter = new FileWriter(mutantpath+logfilename,false)){
					logWriter.append(sb.toString());
				}catch (IOException e) {
					System.out.println(e.toString());
					e.printStackTrace();
					return;
				}
		});
	}
}






//}
//else if (order==2)
//	{
//	sb.append(filename+System.lineSeparator());
//
//	String cmd= (OS.contains("win"))?
//			String.format("cmd /c "+ecdarpath+
//					"verifytga.exe %s "+mutantpath+"query2.q",f.toString()):
//						String.format(ecdarpath+"verifytga %s "+mutantpath+"query3.q",f.toString());
//			try {
//				Process process = Runtime.getRuntime().exec(cmd);
//				BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
//				String s="";
//				while ((s = stdInput.readLine()) != null) {
//					sb.append(s);
//				}
//				sb.append(""+System.lineSeparator()+"");
//			} catch (IOException e) {
//				//  Auto-generated catch block
//				e.printStackTrace();
//			}
//
//}

